# David "Elliott" Perryman
# working with Jan
# looked at for help: wikipedia


#!/usr/bin/env python
# coding: utf-8

# # TUTORIAL 2 : BEZIER CURVES
# ## 1) Bernstein polynomial
# 
# The goal here is to create a function $\textit{Bernstein}(N,t)$ that returns an array of size $(N+1)\times len(t)$. The $i^{th}$ line contains Polynomial $B_N^i(t)$. Its length is the length of $t$.
# You also have to plot the Bernstein polynomials.

# In[2]:


#get_ipython().run_line_magic('matplotlib', 'inline')
from math import factorial
import numpy as np
import matplotlib.pyplot as plt
#----------------------
# Binomial coefficient
def nchoosek(n,p):
    return np.prod(range(n,n-p,-1)) / np.prod(range(p,1,-1))
    return factorial(n) / factorial(p) / factorial(n-p)
# TO BE DONE
#return cnp

#---------------------
# Bernstein Polynomials
# N is the degree
# t = np.linspace(0,1,500)
def Bernstein(N,t):
    return np.array([nchoosek(N,k)*np.power(t,k)*np.power(1-t,N-k) for k in range(N+1)])
# TO BE DONE
 #   return BNt

#----------------------
# plot of the Bernstein polynomials

# ## 2) Interactive acquisition of a control polygon
# The following code does not work in the notebook environment. Please copy/paste it to a file tutorial2.py. 
# From now on, continue everything on this new file.

# In[2]:


def AcquisitionPolygone(minmax,color1,color2) :
    x = []
    y = []
    coord = 0
    while coord != []:
        coord = plt.ginput(1, mouse_add=1, mouse_stop=3, mouse_pop=2)
        if coord != []:
            plt.draw()
            xx = coord[0][0]
            yy = coord[0][1]
            plt.plot(xx,yy,color1,ms=8);
            x.append(xx);
            y.append(yy);
            if len(x) > 1 :
                plt.plot([x[-2],x[-1]],[y[-2],y[-1]],color2)
    #Polygon creation
    Polygon = np.zeros((2,len(x)))
    Polygon[0,:] = x
    Polygon[1,:] = y
    return Polygon

# ## 3) Create a function PlotBezierCurve
# Given a control Polygon, the goal is to plot the Bezier curve using the parametric definition (matrix multiplication)

# In[3]:


def PlotBezierCurve(Polygon):
    # DETERMINE Bezier    
    Polygon = np.array(Polygon) 
    N = len(Polygon[0])-1
    t = np.linspace(0,1,500)
    b_basis = Bernstein(N, t) 
    Bezier = (b_basis.T @ Polygon.T).T 
    plt.draw()
    plt.plot(Bezier[0,:],Bezier[1,:],label='Bezier curve (matrix formulation')
    plt.legend()
    return     


# ## 4) De Casteljau Algorithm

# In[4]:


def DeCasteljau(Pts,t):#returns the two diagonals
    # CALCULATE the two diagonals
    N = len(Pts)
    coef = np.zeros((N,N))
    Pts = np.array(Pts)
    coef[:,0] = Pts
    for i in range(1,N):
        for j in range(N-i):
            coef[j,i]=coef[j,i-1]*(1-t) + coef[j+1,i-1]*t
    return np.array([coef[0], np.diagonal(coef[::-1,:])])

def PlotDeCasteljau(Polygon, alpha):
    t = np.linspace(0,1,500) 
    x = np.array([DeCasteljau(Polygon[0], z) for z in t]) 
    y = np.array([DeCasteljau(Polygon[1], z) for z in t]) 
    plt.draw()
    plt.plot(x[:,-1,-1],y[:,-1,-1], label='de casteljau alg')
    x = DeCasteljau(Polygon[0], alpha) 
    y = DeCasteljau(Polygon[1], alpha) 
    plt.plot(x[0], y[0], label='1st diagonal')
    plt.plot(x[1], y[1], label='2nd diagonal ')
    plt.legend(); 

# ## 5) Plot the first and second subdivision polygon
if __name__=='__main__':
    from sys import argv
    if len(argv)==2:
        alpha=float(argv[1])
    else:
        alpha=0.5
    N=5
    x = np.linspace(0,1,500)
    Bern = Bernstein(N,x)
    for k in range(N+1):
        plt.plot(x,Bern[k])
    plt.title('bernstein polyn. for n=5');
    #plt.show()
    plt.close();

    # main part of the program to call AcquisitionPolygone.
    fig2 = plt.figure()
    ax = fig2.add_subplot(111)    
    minmax = 10
    ax.set_xlim((-minmax,minmax))
    ax.set_ylim((-minmax,minmax))
    plt.title("Polygon acquisition and Bezier curve")
    Poly = AcquisitionPolygone(minmax,'or',':r')
    PlotBezierCurve(Poly)# This function has to be defined 
    PlotDeCasteljau(Poly, alpha)
    plt.waitforbuttonpress()


