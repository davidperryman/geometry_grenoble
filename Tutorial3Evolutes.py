#!/usr/bin/env python
# coding: utf-8

# # TUTORIAL 3 : Evolute of curves
# 
# The evolute of a curve is by definition the set of its centers of curvatures. The goal of this lab session is to define a generic function that computes the evolute of any parameterized curve. 
# 
# ## PART I. 
# We assume in the following that we have a parametrized curve $f:[a,b]\to\mathbb{R}^3$ which is only known through a discretization $(t,f)$ where $$\textit{t = np.linspace(0,1,n)}\quad f=(f(t_0),\cdots,f(t_{n-1})).$$
# $f$ is therefore of size $3 \times n$.
# 
# ## 1) Discrete derivative
# Define a function that calculate an approximation of the derivative of $f$. The output has the same size than $t$ and $f$. 

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

#----------------------
# discrete derivature
def derivative(t,func):
    # TO BE DONE
    h = t[1]-t[0]
    n = len(t)
    deriv = []
    for f in func:
        deriv.append(
            [(-3*f[0]+4*f[1]-f[2])] + 
            [(f[i+1]-f[i-1]) for i in range(1,n-1)] + 
            [(f[-3]-4*f[-2]+3*f[-1])]
        )
    deriv = np.array(deriv)/2/h
    return deriv


def norm(x):
    return np.sqrt(np.sum(x**2,0))


# In[7]:


def normed(x):
    return x/norm(x)


# ## 2) Discrete normal vector
# The goal is to build the function that outputs for every $t_i$ the unit normal $N(t_i)$.

# In[8]:


#----------------------
# discrete derivature
def tangent_vector(t,f):
    # TO BE DONE
    T = normed(derivative(t, f))
    return T


# In[9]:


#----------------------
# discrete derivature
def normal_vector(t,f):
    # TO BE DONE
    N = normed(derivative(t, tangent_vector(t,f)))
    return N

#---------------------
# check that function on simple examples



# ## 3) Curvature function
# The goal is to build the function that outputs for every $t_i$ the curvature $k(t_i)$.

# In[14]:

# In[15]:


#----------------------
# discrete derivature
def curvature(t,f):
    # TO BE DONE
    f_prime = derivative(t,f)
    f_prime_prime = derivative(t,f_prime)
    return np.array([norm(np.cross(fp, fpp))/(norm(fp)**3) for fp, fpp in zip(f_prime.T, f_prime_prime.T)])
    return curvature_f # curvature_f is of size 1 x n

#---------------------
# check that function on simple examples


# In[16]:


# In[17]:

# ## 4) Evolute 
# Define the evolute, namely the set of centers of curvatures. 

# In[18]:


#----------------------
# discrete derivature
def evolute(t,f):
    # TO BE DONE
    N = normal_vector(t,f)
    return f + N/curvature(t,f)
    return evolute_f # evolute_f is of size 3 x n






# ## 3) Evolute of Bezier curves
# Plot the evolute of  Bezier curve that you created before.

# In[27]:


from Tutorial2Bezier import *


# In[28]:



# In[29]:

if __name__== "__main__":
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim((-5,5))
    ax.set_ylim((-5,5))
    Polygon = AcquisitionPolygone(10, 'or', ':r') 
    t = np.linspace(0,1,500) 
    x = np.array([DeCasteljau(Polygon[0], z) for z in t]) 
    y = np.array([DeCasteljau(Polygon[1], z) for z in t]) 
    x, y = x[:,-1,-1], y[:,-1,-1]
    plt.draw(); 
    ev = evolute(t, np.array([x,y]))
    plt.plot(x, y, label='de casteljau alg')
    plt.plot(ev[0], ev[1], label='evolute of bezier curve')
    plt.legend();
    plt.waitforbuttonpress()

# In[ ]:




